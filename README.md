## Getting started

## First step:
Install [Terraform](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli), [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) and [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)


## Second step:
Create folder and go to it.
Run comand:
```
git clone https://gitlab.com/i.kravtsov93/skillbox-final-task3.git
```

## Third step:
Go to folder "terraform" and run command:
```
terraform init
terraform apply --auto-approve
```

## Fourth step:
Go to folder "ansible" and run command:

For deploy Prometheus run:
```
ansible-playbook prom.yaml -b
```
For deploy grafana run:
```
ansible-playbook grafana.yaml -b
```
For deploy test_app run:
```
ansible-playbook deploy_test.yaml -b
```