terraform {
    required_providers {
      yandex = {
        source = "yandex-cloud/yandex"
      }
    }
  required_version = ">= 0.13"
}

provider "yandex" {
    token       = var.token
    cloud_id    = var.cloud_id
    folder_id   = var.folder_id
    zone        = var.zone
}

data "yandex_compute_image" "ubuntu-2204-1" {
    family = "ubuntu-2204-lts"
}

#Create Prometheus
resource "yandex_compute_instance" "prometheus" {
    name        = "prometheus"
    platform_id = "standard-v1"
    zone        = "ru-central1-a"

    resources {
        cores  = 2
        memory = 4
    }

    boot_disk {
        initialize_params {
            image_id = "${data.yandex_compute_image.ubuntu-2204-1.id}"
            size = "10"
        }
    }

    network_interface {
        subnet_id = yandex_vpc_subnet.sn-1.id
        nat = true
    }

    metadata = {
        ssh-keys = "ubuntu:${file("/home/ikravtsov/.ssh/id_rsa.pub")}"
    }
}

#Create Grafana
resource "yandex_compute_instance" "grafana" {
    name        = "grafana"
    platform_id = "standard-v1"
    zone        = "ru-central1-a"

    resources {
        cores  = 2
        memory = 4
    }

    boot_disk {
        initialize_params {
            image_id = "${data.yandex_compute_image.ubuntu-2204-1.id}"
            size = "10"
        }
    }

    network_interface {
        subnet_id = yandex_vpc_subnet.sn-1.id
        nat = true
    }

    metadata = {
        ssh-keys = "ubuntu:${file("/home/ikravtsov/.ssh/id_rsa.pub")}"
    }
}

#Create test
resource "yandex_compute_instance" "vm-test-1"{
    name        = "test-1"
    platform_id = "standard-v1"
    zone        = "ru-central1-a"

    resources {
        cores   = 2
        memory  = 2
    }

    boot_disk {
        initialize_params {
            image_id = "${data.yandex_compute_image.ubuntu-2204-1.id}"          
        }
    }

    network_interface {
        subnet_id = yandex_vpc_subnet.sn-1.id
        nat = true
    }

    metadata = {
        ssh-keys = "ubuntu:${file("/home/ikravtsov/.ssh/id_rsa.pub")}"
    }
}

resource "yandex_vpc_network" "default" {
    name = "network-1"
}

resource "yandex_vpc_subnet" "sn-1" {
    zone            = "ru-central1-a"
    network_id      = yandex_vpc_network.default.id
    v4_cidr_blocks  = ["10.1.10.0/24"]
}

output "internal_ip_address_prometheus" {
  value = yandex_compute_instance.prometheus.network_interface.0.nat_ip_address 
}

output "internal_ip_address_grafana" {
  value = yandex_compute_instance.grafana.network_interface.0.nat_ip_address 
}

output "internal_ip_address_vm-test-1" {
  value = yandex_compute_instance.vm-test-1.network_interface.0.nat_ip_address 
}