## Status
Active

## Context
Monitoring systems monitor the technologies used by company (equipment, networks and communications, operating systems or applications, etc.) to analyze their operation and performance, as well as to detect and warn about possible errors.

This will help us in identifying and analyzing bottlenecks in software development.

## Decision
To carry out monitoring, we will use Prometheus, a monitoring system, and Grafana, a data vizualization system.

## Consequences
We need to set up and maintain the Prometheus + Grafana monitoring system, which will entail small costs, but will help avoid problems with performance, etc.